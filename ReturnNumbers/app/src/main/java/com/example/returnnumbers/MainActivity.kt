package com.example.returnnumbers
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.returnnumbers.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
    }
    fun clickLogIn(view: View)
    {
        //val snack:Snackbar=

        if(isValidEmail(binding.usernameEditText.text.toString())  && !binding.passwordEditText.text.toString().isEmpty())
        {

            val intent:Intent=Intent(this,MainActivity2::class.java)
            startActivity(intent)
        }

        else
        {
            Toast.makeText(this, getString(R.string.validUserName), Toast.LENGTH_SHORT).show()
        }
    }


    fun isValidEmail(target: String): Boolean {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
    }
}
